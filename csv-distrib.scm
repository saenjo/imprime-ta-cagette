(use-modules (dsv)
             (srfi srfi-1)
             (srfi srfi-9)
             (srfi srfi-19)
             (ice-9 match)
             (ice-9 i18n)
             (ice-9 vlist)

             (skribilo)
             (skribilo engine)
             (skribilo evaluator)
             (skribilo output)
             (skribilo package base))

(define (string->object str)
  (call-with-input-string str read))

(define fr
  (make-locale LC_ALL "fr_FR.utf8"))

(define (french-number-string->number str)        ;terrible hack
  (string->number
   (string-map (match-lambda
                 (#\, #\.)
                 (chr chr))
               str)))

(define-record-type <product>
  (make-product name amount price)
  product?
  (name      product-name)
  (amount    product-amount)
  (price     product-price))

(define-record-type <bundle>
  (make-bundle people products)
  bundle?
  (people    bundle-people)
  (products  bundle-products))

(define (read-entries port)
  ;; List of name/value tuples.
  (match (dsv->scm port #\;)
    (((names products prices amounts _ ...) ...)
     (filter-map (lambda (name product price amount)
                   (and (not (string=? name "Nom"))
                        (make-bundle (string->object name)
                                     (list (make-product
                                            (string->object product)
                                            (french-number-string->number
                                             (string->object amount))
                                            (french-number-string->number
                                             (string->object price)))))))
                 names products prices amounts))))

(define (coalesce-bundles bundles)
  (define table
    (make-hash-table))

  (for-each (match-lambda
              (($ <bundle> people products)
               (let ((rest (hash-ref table people '())))
                 (hash-set! table people
                            (append products rest)))))
            bundles)
  (sort (hash-fold (lambda (people products lst)
                     (cons (make-bundle people products) lst))
                   '()
                   table)
        (match-lambda*
          ((($ <bundle> name1 _) ($ <bundle> name2 _))
           (string-locale-ci<? name1 name2 fr)))))


(define doc
  (document #:title "Liste de distribution PETAL Brama"
            #:date "mai 2021"

            (p
             (table
              ;; #:frame 'below
              ;; #:rules 'rows
              (tr (th (bold "Nom")) (th (bold "Quantité")))
              (append-map (match-lambda
                            (($ <bundle> people products)
                             `(,(tr #:bg #xffeeee
                                    (th #:align 'left #:colspan 2
                                        (bold people)))
                               ,@(map (lambda (product)
                                        (tr (td #:align 'right
                                                #:rules 'above
                                                (product-name product))
                                            (td #:align 'right
                                                (number->locale-string
                                                 (product-amount product)
                                                 0 fr))))
                                      products))))
                          (coalesce-bundles (read-entries (current-input-port))))))))

(let ((engine (find-engine 'lout)))
  (setlocale LC_ALL "fr_FR.utf8")
  (engine-custom-set! engine 'initial-language "French")
  (engine-custom-set! engine 'date-line
                      (or (getenv "DISTRIB_DATE")
                          (date->string (time-utc->date
                                         (current-time time-utc))
                                        "~d ~B ~Y")))
  (output (evaluate-document doc engine) engine))
