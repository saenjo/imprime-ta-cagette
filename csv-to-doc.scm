(use-modules (dsv)
             (srfi srfi-1)
             (srfi srfi-19)
             (ice-9 match)
             (ice-9 i18n)
             (ice-9 vlist)

             (skribilo)
             (skribilo engine)
             (skribilo evaluator)
             (skribilo output)
             (skribilo package base))

(define (string->object str)
  (call-with-input-string str read))

(define fr
  (make-locale LC_ALL "fr_FR.utf8"))

(define (french-number-string->number str)        ;terrible hack
  (string->number
   (string-map (match-lambda
                 (#\, #\.)
                 (chr chr))
               str)))

(define (fetch-entries port)
  ;; List of name/value tuples.
  (match (dsv->scm port #\;)
    (((names products prices amounts _ ...) ...)
     (filter-map (lambda (name amount price)
                   (and (not (string=? name "Nom"))
                        (list (string->object name)
                              (* (french-number-string->number
                                  (string->object amount))
                                 (french-number-string->number
                                  (string->object price))))))
                 names amounts prices))))

(define (checks entries)
  ;; List of name/value tuples with totals, sorted.
  (let ((table (make-hash-table)))
    (for-each (match-lambda
                ((name value)
                 (let ((total (hash-ref table name 0.)))
                   (hash-set! table name (+ total value)))))
              entries)
    (sort (hash-fold (lambda (name value lst)
                       (cons (list name value) lst))
                     '()
                     table)
          (match-lambda*
            (((name1 _) (name2 _))
             (string-locale-ci<? name1 name2 fr))))))


(define doc
  (document #:title "Liste de chèques PETAL Brama"
            #:date "avril 2021"

            (table
             (tr (th "nom") (th "somme"))
             (map (match-lambda
                    ((name value)
                     (tr (td #:align 'left name)
                         (td #:align 'right
                             (monetary-amount->locale-string
                              value #t fr)))))
                  (checks (fetch-entries (current-input-port)))))))

(let ((engine (find-engine 'lout)))
  (setlocale LC_ALL "fr_FR.utf8")
  (engine-custom-set! engine 'initial-language "French")
  (engine-custom-set! engine 'date-line
                      (or (getenv "DISTRIB_DATE")
                          (date->string (time-utc->date
                                         (current-time time-utc))
                                        "~d ~B ~Y")))
  (output (evaluate-document doc engine) engine))
